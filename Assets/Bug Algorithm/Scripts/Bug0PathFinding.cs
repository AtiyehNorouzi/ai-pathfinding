﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Bug0PathFinding  : MonoBehaviour , IBugPathFinding 
{
    #region FIELDS
    private Vector3 _targetPos;
    private CharacterMove _character;
    private Vector3 _raycastDir;
    private Vector3 _moveDir;
    private  MoveState _currentState = MoveState.HeadGoal;
    #endregion

    #region ENUMS
    enum MoveState { HeadGoal=0, FollowWall }
    #endregion

    #region MONO BEHAVIOURS
    public IEnumerator HeadToGoal()
    {
        while(true)
        {
            if (_currentState == MoveState.HeadGoal)
            {
                if (Vector3.Distance(_character.transform.position, _targetPos) <= 0.5f)// check if character reached target
                    yield break;

                if(!HeadTowardGoal())//if character hits an obstacle , it follows the wall
                {
                    _currentState = MoveState.FollowWall;
                    _moveDir = PathFindingUtility.CalculateRotation(_character, _raycastDir);
                }
            }
            yield return null;
        }
    }

    public IEnumerator FollowWall()
    {
        while(true)
        {
            if (Vector3.Distance(_character.transform.position, _targetPos) <= 0.5f)// check if character reached target
                yield break;

            _character.transform.Translate(Vector3.up * 5f * Time.deltaTime);
            yield return null;
        }
    }

    public IEnumerator CheckIfObstacleEnds()
    {
        while(true)
        {
            if (_currentState == MoveState.FollowWall) // if state is follow wall, each 0.2f seconds check whether the characters can head toward character or not.
            {
                if (PathFindingUtility.CheckCanChangeDirection(_character , _raycastDir , 4f))
                {
                    Vector3 direction = PathFindingUtility.CalculateDirection(_character.transform.position, _targetPos);
                    RaycastHit2D rch = Physics2D.Raycast(_character.transform.position, direction, 3f, LayerMask.GetMask("Default"));
                    if (rch.collider == null)
                    {
                        _currentState = MoveState.HeadGoal;
                    }
                    else
                    {
                        Vector3 temp = _moveDir;
                        _moveDir = _raycastDir;
                        _raycastDir = -temp;
                        PathFindingUtility.ChangeRotation(_moveDir, _character.gameObject);
                    }
                }
            }
            yield return new WaitForSeconds(0.3f);
        }
    }
    #endregion

    #region PUBLIC METHODS
    public void FindPath(CharacterMove character)
    {
        this._character = character;
        StartCoroutine(HeadToGoal());
        StartCoroutine(FollowWall());
        StartCoroutine(CheckIfObstacleEnds());
    }

    public bool HeadTowardGoal()
    {
        Vector3 direction = PathFindingUtility.CalculateDirection(_character.transform.position, _targetPos);
        RaycastHit2D raycasthit = Physics2D.Raycast(_character.transform.position, direction, 1f, LayerMask.GetMask("Default"));
        if (raycasthit.collider == null)
        {
            PathFindingUtility.ChangeRotation(direction, _character.gameObject);
            return true;
        }
        _raycastDir = PathFindingUtility.NormalizedDir(PathFindingUtility.CalculateDirection(_character.transform.position, raycasthit.point));
        return false;

    }

    public void SetTarget(Vector3 targetPos)
    {
        _targetPos = targetPos;
    }
    #endregion

}
