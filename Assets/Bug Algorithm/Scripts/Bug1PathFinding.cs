﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bug1PathFinding : MonoBehaviour , IBugPathFinding
{
    #region FIELDS
    private List<Vector3> _dirMemory = new List<Vector3>();
    private Vector3 _targetPos;
    private CharacterMove _character;
    private Vector3 _raycastDir;
    private Vector3 _moveDir;
    private MoveState _currentState = MoveState.HeadGoal;
    private bool _roundObstacle = false;
    private Vector3 _nearestPoint;
    private Vector3 _startPoint;
    private bool _reachNearestPoint = false;
    #endregion

    #region ENUM
    enum MoveState
    {
        HeadGoal = 0, FollowWall
    }
    #endregion

    #region MONO BEHAVIOURS
    public IEnumerator HeadToGoal()
    {
        while (true)
        {
            if (_currentState == MoveState.HeadGoal)
            {
                if (PathFindingUtility.GetDistance(_character.transform.position, _targetPos) <= 0.5f)// check if character reached target
                    yield break;

                if (!HeadTowardGoal())//if character hits an obstacle , it rounds around obstacle to find nearest point to target
                {
                    _roundObstacle = false;
                    _currentState = MoveState.FollowWall;  
                    _moveDir = PathFindingUtility.CalculateRotation(_character, _raycastDir);
                    _dirMemory.Add(_moveDir);
                }
            }
            yield return null;
        }
    }
    public IEnumerator FollowWall()
    {
        while (true)
        {
            if (PathFindingUtility.GetDistance(_character.transform.position, _targetPos) <= 0.5f)// check if character reached target
                yield break;

            if (_currentState == MoveState.FollowWall)
            {
                float dist = Vector3.Distance(_character.transform.position, _targetPos);
                if (dist <= Vector3.Distance(_nearestPoint, _targetPos))
                    _nearestPoint = _character.transform.position;

                if (PathFindingUtility.GetDistance(_character.transform.position , _startPoint) <= 0.5f && _dirMemory.Count >= 3) // check if character reached startpoint for the second time
                    _reachNearestPoint = true;

                if(_reachNearestPoint)
                {
                    if (PathFindingUtility.GetDistance(_character.transform.position, _nearestPoint) <= 0.5f)
                    {
                        _roundObstacle = true;
                        _currentState = MoveState.HeadGoal;
                        _reachNearestPoint = false;
                        _dirMemory.Clear();
                    }
                }
            }
            _character.transform.Translate(Vector3.up * 5f * Time.deltaTime);
            yield return null;
        }
    }

    public IEnumerator CheckIfObstacleEnds()
    {
        while (true)
        {
            if (_currentState == MoveState.FollowWall)
            {
                if (PathFindingUtility.CheckCanChangeDirection(_character, _raycastDir , 5f))
                {
                    if (_roundObstacle)
                        _currentState = MoveState.HeadGoal;
                    else
                    {
                        _raycastDir = -_dirMemory[_dirMemory.Count - 1];
                        Vector3 direction = PathFindingUtility.FindNearestDir(_dirMemory , _character.transform.position , _targetPos);
                        _dirMemory.Add(direction);
                        PathFindingUtility.ChangeRotation(direction, _character.gameObject);
                    }
                }
            }
  
            yield return new WaitForSeconds(0.3f);
        }
    }
    #endregion

    #region PUBLIC METHODS
    public void FindPath(CharacterMove character)
    {
        this._character = character;
        StartCoroutine(HeadToGoal());
        StartCoroutine(FollowWall());
        StartCoroutine(CheckIfObstacleEnds());
    }

    public bool HeadTowardGoal()
    {
        Vector3 direction = PathFindingUtility.CalculateDirection(_character.transform.position, _targetPos);
        RaycastHit2D raycasthit = Physics2D.Raycast(_character.transform.position, direction, 0.8f, LayerMask.GetMask("Default"));
        if (raycasthit.collider == null)
        {
            PathFindingUtility.ChangeRotation(direction, _character.gameObject);
            return true;
        }
        _startPoint = _character.transform.position;
        _nearestPoint = _startPoint;
        _raycastDir = PathFindingUtility.NormalizedDir(PathFindingUtility.CalculateDirection(_character.transform.position, raycasthit.point));
        return false;
    }

  
    public void SetTarget(Vector3 targetPos)
    {
        _targetPos = targetPos;
    }

    #endregion

}
