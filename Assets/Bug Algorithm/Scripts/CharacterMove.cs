﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CharacterMove : MonoBehaviour
{
    #region FIELDS
    public Transform LeftHand;

    public Transform RightHand;

    [SerializeField]
    private Dropdown _bugDropdown;

    [SerializeField]
    private LineRenderer _lineRenderer;

    [SerializeField]
    private GameObject _target;

    [SerializeField]
    private Bug0PathFinding bug0;

    [SerializeField]
    private Bug1PathFinding bug1;

    [SerializeField]
    private Bug2PathFinding bug2;

    private int _algorithm;

    private IBugPathFinding _pathFinding;

    private Vector3 _lastPointRenderered;

    private int _rendererIndex = 0;
    #endregion

    #region MONO BEHAVIOURS
    private void Update()
    {
        if (_pathFinding == null)
            return;
      
        RenderLine();
    }
    #endregion

    #region PUBLIC METHODS
    public void BugAlgorithm()
    {
        _algorithm = _bugDropdown.value;
        if (_algorithm == 0)
            _pathFinding = bug0;
        else if (_algorithm == 1)
            _pathFinding = bug1;
        else
            _pathFinding = bug2;

        _pathFinding.SetTarget(_target.transform.position);
        _pathFinding.FindPath(this);
        _lineRenderer.SetPosition(_rendererIndex++, transform.position);
        _lastPointRenderered = transform.position; 
    }
    #endregion

    #region PRIVATE METHODS
    private void RenderLine()
    {
        if (CalculateDistance(transform.position, _lastPointRenderered))
        {
            _lineRenderer.positionCount++;
            _lineRenderer.SetPosition(_rendererIndex++, transform.position);
            _lastPointRenderered = transform.position;
        }
    }
    private bool CalculateDistance(Vector3 posNow, Vector3 prevPos)
    {
        if (Vector3.Distance(posNow, prevPos) >= 0.3f)
            return true;
        return false;
    }
    #endregion

}
