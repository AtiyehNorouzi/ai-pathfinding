﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBugPathFinding 
{
    IEnumerator HeadToGoal();
    IEnumerator FollowWall();
    IEnumerator CheckIfObstacleEnds();
    bool HeadTowardGoal();
    void FindPath(CharacterMove character);
    void SetTarget(Vector3 targetPos);
}
